var gulp =        require('gulp');
var sass =        require('gulp-sass');
var cssnano =     require('gulp-cssnano');
var uglify =      require('gulp-uglify');
var rename =      require('gulp-rename');
var sourcemaps =  require('gulp-sourcemaps');
var pump =        require('pump');

var JS_PATTERN = ['assets/js/**/*.js', '!assets/js/**/*.min.*'];
var CSS_PATTERN = 'assets/scss/**/*.scss';
var MIN_SUFFIX = {suffix: '.min'};

gulp.task('sass', function() {
  pump([
    gulp.src(CSS_PATTERN),
    sourcemaps.init(),
    sass(),
    cssnano(),
    rename(MIN_SUFFIX),
    sourcemaps.write('.'),
    gulp.dest('assets/css')
  ])
});

gulp.task('scripts', function() {
  pump([
    gulp.src(JS_PATTERN),
    sourcemaps.init(),
    uglify(),
    rename(MIN_SUFFIX),
    sourcemaps.write('.'),
    gulp.dest('assets/js')
  ])
})

gulp.task('watch', function() {
  gulp.watch(CSS_PATTERN, ['sass']);
  gulp.watch(JS_PATTERN, ['scripts']);
});

gulp.task('default', ['sass', 'scripts']);