<?php
/*
Plugin Name: Custom Login Page - Casey Morris
Description: Customize the WordPress login page
Version: 1.0.0
Author: Casey Morris
Author URI: http://caseymorris.io
Text Domain: custom-login-page-cmorris
*/

if ( ! class_exists('CM_Custom_Login') ) {
  class CM_Custom_Login {
    const LOST_PASSWORD_TEXT    = "Forgot password?";
    const REMEMBER_ME_TEXT      = "Save Credentials";
    const LOGIN_BTN_TEXT        = "Login";
    const BACK_TO_LOGIN_TEXT    = "Back to login";
    const USERNAME_INPUT_LABEL  = "Username";
    const PASSWORD_INPUT_LABEL  = "Password";
    const CHECK_REMEMBER_ME_URL = 'assets/js/checkRememberMe.min.js';
    const CUSTOM_STYLES_URL     = 'assets/css/cm-custom-login-page-styles.min.css';
    const MONTSERRAT_FONT_URL   = 'https://fonts.googleapis.com/css?family=Montserrat:400,700';
    const AMIRI_FONT_URL        = 'https://fonts.googleapis.com/css?family=Amiri:400i';
    const DISABLE_ERROR_SHAKE   = true;
    const REMEMBER_ME_CHECKED   = true;
    const USE_CUSTOM_ERRORS     = true;

    static $CUSTOM_ERRORS = array(
      'error_prefix' => '<strong>Oops!</strong> ',
      'invalid_username' => 'No user by that name.',
      'incorrect_password' => 'Password incorrect.',
      'empty_password' => 'No password entered.',
      'empty_username' => 'No username entered.'
    );

    /**
     * Holds instance of singleton
     *
     */

    static $instance = false;

    /**
     * Constructor, inits hooks and enqueues styles
     *
     * @return void
     */

    private function __construct() {
      self::add_custom_styles();

      add_filter('login_headertitle', array($this, 'change_logo_title'));
      add_filter('login_headerurl',   array($this, 'change_site_url'));
      add_filter('login_footer',      array($this, 'check_remember_me'));
      add_filter('login_errors',      array($this, 'change_error_messages'));
      add_filter('gettext',           array($this, 'change_lostpassword_text'));
      add_filter('gettext',           array($this, 'change_rememberme_text'));
      add_filter('gettext',           array($this, 'change_backtologin_btn_text'));
      add_filter('gettext',           array($this, 'change_login_btn_text'));
      add_filter('gettext',           array($this, 'change_username_label_text'));
      add_filter('gettext',           array($this, 'change_password_label_text'));
      add_action('login_head',        array($this, 'remove_error_shake'));      
    }

    /**
     * If instance already exists, return instance, otherwise create and return instance
     *
     * @return CM_Custom_Login
     */

    public static function getInstance() {
      if ( !self::$instance ) {
        self::$instance = new self;
      }

      return self::$instance;
    }

    /**
     * Register/enqueue fonts and styles
     * Montserrat: Alternative to Proxima Nova (used on 10up.com)
     * Amiri: Alternative to Freight Text Pro (used on 10up.com)
     *
     * @return void
     */

    public static function add_custom_styles() {
      wp_enqueue_style('custom_login_styles', plugins_url(self::CUSTOM_STYLES_URL, __FILE__));
      wp_enqueue_style('montserrat-font', self::MONTSERRAT_FONT_URL);
      wp_enqueue_style('amiri-font', self::AMIRI_FONT_URL);
    }

    /**
     * Returns the site url, used to change logo href location from wordpress.com
     *
     * @return string
     */

    public static function change_site_url() {
      return get_bloginfo('url');
    }

    /**
     * Returns the site title, for use in logo title attribute
     *
     * @return string
     */

    public static function change_logo_title() {
      return get_bloginfo('name');
    }

    /**
     * Checks the remember me checkbox
     *
     * @return void
     */

    public static function check_remember_me() {
      if (self::REMEMBER_ME_CHECKED) {
        wp_enqueue_script('check-remember-me', plugins_url(self::CHECK_REMEMBER_ME_URL, __FILE__), array('jquery'));
      }
    }

    /**
     * Removes shake effect on form error
     *
     * @return void
     */

    public static function remove_error_shake() {
      if (self::DISABLE_ERROR_SHAKE) {
        remove_action('login_head', 'wp_shake_js', 12);
      }
    }

    /**
     * Changes user/password error messages
     *
     * @param   string $error
     * @return  void
     */

    public static function change_error_messages($error) {
      if ( self::USE_CUSTOM_ERRORS ) {
        global $errors;
        $err_codes = $errors->get_error_codes();

        // run error through series of conditionals
        $error = self::change_error_message('invalid_username', $err_codes, $error);
        $error = self::change_error_message('incorrect_password', $err_codes, $error);
        $error = self::change_error_message('empty_username', $err_codes, $error);
        $error = self::change_error_message('empty_password', $err_codes, $error);
      }

      return $error;
    }

    /**
     * Changes error message text
     *
     * @param   string  $error_to_check
     * @param   array   $err_codes
     * @param   string  $original_error
     * @return  string
     */

    public static function change_error_message($error_to_check, $err_codes, $original_error) {
      if ( in_array($error_to_check, $err_codes)) {
        return self::$CUSTOM_ERRORS['error_prefix'] . self::$CUSTOM_ERRORS[$error_to_check];
      } else {
        return $original_error;
      }
    }

    /**
     * Replaces text, used in conjuction with `gettext` hook
     *
     * @param   string $words
     * @param   string $text
     * @param   string $replace_with
     * @return  string
     */

    public static function replace_words($words, $text, $replace_with) {
      if ($text == $words) { $text = $replace_with; }
      return $text;
    }

    /**
     * Replaces `Lost your password?` link text
     *
     * @param   string $text
     * @return  string
     */

    public static function change_lostpassword_text($text) {
      return self::replace_words('Lost your password?', $text, self::LOST_PASSWORD_TEXT);
    }

    /**
     * Replaces `Remember Me` checkbox text
     *
     * @param   string $text
     * @return  string
     */

    public static function change_rememberme_text($text) {
      return self::replace_words('Remember Me', $text, self::REMEMBER_ME_TEXT);
    }

    /**
     * Replaces `Log in` link text
     * This is the link that sends you back to the login page from lost password
     *
     * @param   string $text
     * @return  string
     */

    public static function change_backtologin_btn_text($text) {
      return self::replace_words('Log in', $text, self::BACK_TO_LOGIN_TEXT);
    }

    /**
     * Replaces `Log In` button text
     * This is the button that you use to log in (submit)
     *
     * @param   string $text
     * @return  string
     */

    public static function change_login_btn_text($text) {
      return self::replace_words('Log In', $text, self::LOGIN_BTN_TEXT);
    }

    /**
     * Replaces `Username or Email Address` label text
     *
     * @param   string $text
     * @return  string
     */

    public static function change_username_label_text($text) {
      return self::replace_words('Username or Email Address', $text, self::USERNAME_INPUT_LABEL);
    }

    /**
     * Replaces `Password` label text
     *
     * @param   string $text
     * @return  string
     */

    public static function change_password_label_text($text) {
      return self::replace_words('Password', $text, self::PASSWORD_INPUT_LABEL);
    }

  }
}

// Instantiate plugin class
$CM_Custom_Login = CM_Custom_Login::getInstance();